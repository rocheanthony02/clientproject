package com.jacidi.project.jacidi.project.dao;

import org.springframework.data.repository.CrudRepository;

import com.jacidi.project.jacidi.project.entities.Product;

public interface IProductDao extends CrudRepository<Product, Long> {

}
