package com.jacidi.project.jacidi.project.dao;

import org.springframework.data.repository.CrudRepository;

import com.jacidi.project.jacidi.project.entities.Client;

public interface IClientDao extends CrudRepository<Client, Long> {

}
