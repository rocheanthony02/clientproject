package com.jacidi.project.jacidi.project.dao;

import org.springframework.data.repository.CrudRepository;
import com.jacidi.project.jacidi.project.entities.Membership;

public interface IMembershipDao extends CrudRepository<Membership, Long>{

}
