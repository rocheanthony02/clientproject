package com.jacidi.project.jacidi.project.dao;

import org.springframework.data.repository.CrudRepository;

import com.jacidi.project.jacidi.project.entities.Shipment;

public interface IShipmentDao extends CrudRepository<Shipment, Long>{
}
