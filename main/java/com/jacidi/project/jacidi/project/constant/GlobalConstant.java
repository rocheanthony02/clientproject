package com.jacidi.project.jacidi.project.constant;

public enum GlobalConstant {
	
	MENSSAGE_ERROR("Error ! se produjo un error al realizar la operacion: "),
	MESSAGE_SUCCESS("Operacion realizada exitosamente"),
	MESSAGE_NOT_FOUND("El registro no se encuentra");
	
	private final String mensaje;
	
	GlobalConstant(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

}
