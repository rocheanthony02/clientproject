package com.jacidi.project.jacidi.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jacidi.project.jacidi.project.dao.IShipmentDao;
import com.jacidi.project.jacidi.project.entities.Product;
import com.jacidi.project.jacidi.project.entities.Shipment;

@Service
public class ShipmentServiceImpl implements IShipmentService {

	@Autowired
	private IShipmentDao shipmentDao;

	@Override
	public List<Shipment> findAll() {
		return (List<Shipment>) this.shipmentDao.findAll();
	}

	@Override
	@Transactional
	public Shipment save(Shipment shipment) {
		
		// obtenemos el totalCost de shipment (en este caso es 0)
		Double totalCost = shipment.getTotalCost();

		for (Product p : shipment.getProducts()) {
			totalCost += p.getCost();
		}

		shipment.setTotalCost(totalCost);
		shipment.setDeliveryDate(shipment.deliveryDate());

		return this.shipmentDao.save(shipment);

	}

	@Override
	public List<Shipment> removeProduct(Product product) {
		List<Shipment> shipments = (List<Shipment>) this.shipmentDao.findAll();
		
		for(Shipment s: shipments) {
			for(Product p: s.getProducts()) {
				if(p.getId() == product.getId()) {
					s.getProducts().remove(product);
				}
			}
		}
		
		
		return shipments;
	}

	@Override
	public Shipment findOne(Long id) {
		return this.shipmentDao.findById(id).orElse(null);
	}

}
