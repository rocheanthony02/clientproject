package com.jacidi.project.jacidi.project.services;

import java.util.List;

import com.jacidi.project.jacidi.project.entities.Membership;

public interface IMembershipService {
	
	List<Membership> findAll();
	
	Membership findOne(Long id);
	
	Membership save(Membership membership);
	
	void delete(Long id);
	
}
