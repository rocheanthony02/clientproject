package com.jacidi.project.jacidi.project.services;

import java.util.List;

import com.jacidi.project.jacidi.project.entities.Client;

public interface IClientService {
	
	List<Client> findAll();
	
	Client findOne(Long id);
	
	Client save(Client client);
	
	void delete(Long id);

}
