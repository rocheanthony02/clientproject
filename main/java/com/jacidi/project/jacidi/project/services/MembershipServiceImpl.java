package com.jacidi.project.jacidi.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jacidi.project.jacidi.project.dao.IMembershipDao;
import com.jacidi.project.jacidi.project.entities.Membership;

@Service
public class MembershipServiceImpl implements IMembershipService {
	
	@Autowired
	private IMembershipDao membershipDao;

	@Override
	public List<Membership> findAll() {
		return (List<Membership>) this.membershipDao.findAll();
	}

	@Override
	public Membership findOne(Long id) {
		return this.membershipDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Membership save(Membership membership) {
		return this.membershipDao.save(membership);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		this.membershipDao.delete(this.findOne(id));
	}

}
