package com.jacidi.project.jacidi.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jacidi.project.jacidi.project.dao.IProductDao;
import com.jacidi.project.jacidi.project.entities.Product;

@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private IProductDao productDao;

	@Override
	public List<Product> findAll() {
		return (List<Product>) this.productDao.findAll();
	}

	@Override
	public Product findOne(Long id) {
		return this.productDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Product save(Product product) {
		return this.productDao.save(product);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		this.productDao.delete(this.findOne(id));
	}

}
