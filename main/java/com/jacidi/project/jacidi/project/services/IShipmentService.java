package com.jacidi.project.jacidi.project.services;

import java.util.List;

import com.jacidi.project.jacidi.project.entities.Product;
import com.jacidi.project.jacidi.project.entities.Shipment;

public interface IShipmentService {
	
	List<Shipment> findAll();
	
	Shipment findOne(Long id);
	
	Shipment save(Shipment shipment);
	
	List<Shipment> removeProduct(Product product);

}
