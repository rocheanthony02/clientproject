package com.jacidi.project.jacidi.project.services;

import java.time.LocalDateTime;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jacidi.project.jacidi.project.dao.IClientDao;
import com.jacidi.project.jacidi.project.entities.Client;

@Service
public class ClientServiceImpl implements IClientService{
	
	@Autowired
	private IClientDao clientDao;

	@Override
	public List<Client> findAll() {
		return (List<Client>) this.clientDao.findAll();
	}

	@Override
	public Client findOne(Long id) {
		return this.clientDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Client save(Client client) {
		
		if(client.getId() == null) {
			Client clientPersisted = this.clientDao.save(client);
			clientPersisted.setNextRenewal(clientPersisted.getNextRenewal().plusDays(clientPersisted.getMembership().getDuration()));
			
			return clientPersisted;
		}else {
			client.setNextRenewal(LocalDateTime.now().plusDays(client.getMembership().getDuration()));
			return this.clientDao.save(client);
		}
		
	}

	@Override
	@Transactional
	public void delete(Long id) {
		this.clientDao.delete(this.findOne(id));
	}
}
