package com.jacidi.project.jacidi.project.services;

import java.util.List;

import com.jacidi.project.jacidi.project.entities.Product;

public interface IProductService {
	
	List<Product> findAll();
	
	Product findOne(Long id);
	
	Product save(Product product);
	
	void delete(Long id);

}
