package com.jacidi.project.jacidi.project.entities;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "shipment")
public class Shipment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double totalCost;
	private LocalDateTime deliveryDate;
	
	@ManyToOne
	@JoinColumn(name = "id_client", referencedColumnName = "id")
	private Client client;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_product", referencedColumnName = "id", insertable = true, updatable = true)
	private List<Product> products;
	
	public Shipment() {
		this.products = new ArrayList<>();
		
	}
	
	public LocalDateTime deliveryDate() {
		if(this.getClient().getMembership().getPrio() > 0 && this.getClient().getMembership().getPrio() <= 50) {
			return LocalDateTime.now().plusDays(1);
		}else if(this.getClient().getMembership().getPrio() > 50 && this.getClient().getMembership().getPrio() <= 80) {
			return LocalDateTime.now().plusHours(12);
		}else {
			return LocalDateTime.now().plusHours(1);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public LocalDateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "Shipment [id=" + id + ", totalCost=" + totalCost + ", deliveryDate=" + deliveryDate + ", client="
				+ client + "]";
	}

}
