package com.jacidi.project.jacidi.project.entities;

import java.time.LocalDateTime; 

import javax.persistence.*;

@Entity
@Table(name = "client")
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private String lastName;
	
	@Column(unique = true)
	private String email;
	
	private LocalDateTime lastDelivery;
	
	private LocalDateTime nextRenewal;
	
	@JoinColumn(name = "id_membership", referencedColumnName = "id")
	@ManyToOne
	private Membership membership;
	
	@PrePersist
	public void prePersist() {
		this.nextRenewal = LocalDateTime.now();
	}
	
	public Client() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getLastDelivery() {
		return lastDelivery;
	}

	public void setLastDelivery(LocalDateTime lastDelivery) {
		this.lastDelivery = lastDelivery;
	}

	public LocalDateTime getNextRenewal() {
		return nextRenewal;
	}

	public void setNextRenewal(LocalDateTime nextRenewal) {
		this.nextRenewal = nextRenewal;
	}

	

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", lastName=" + lastName + ", email=" + email + ", lastDelivery="
				+ lastDelivery + ", nextRenewal=" + nextRenewal + ", membership=" + membership + "]";
	}
	
}
