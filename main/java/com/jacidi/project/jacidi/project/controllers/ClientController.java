package com.jacidi.project.jacidi.project.controllers;

import java.time.LocalDateTime;
import java.util.*;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@Api(name = "Servicio Client",
		description = "Client", group = "API")
@ApiVersion(since = "1.0")
@RestController
@CrossOrigin("*")
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	private ClientServiceImpl clientService;
	
	@Autowired
	private MembershipServiceImpl membershipService;
	
	@GetMapping("/show")
	public List<Client> showClients(){
		return this.clientService.findAll();
	}
	
	@GetMapping("/show/{id}")
	public ResponseEntity<?> showOneClient(@PathVariable Long id){
		
		Client client = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			client = this.clientService.findOne(id);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		if(client == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Client>(client, HttpStatus.OK);
		
	}
	
	@PostMapping("/")
	public ResponseEntity<?> createClient(@RequestBody Client client){
		
		Client clientNew = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			clientNew = this.clientService.save(client);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Cliente Nuevo", clientNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateClient(@RequestBody Client client, @PathVariable Long id){
		
		Client clientFound = this.clientService.findOne(id);
		Client clientUpdated = null;
		Map<String, Object> response = new HashMap<>();
		
		if(clientFound == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			clientFound.setName(client.getName());
			clientFound.setLastName(client.getLastName());
			clientFound.setEmail(client.getEmail());
			clientFound.setLastDelivery(client.getLastDelivery());
			clientFound.setNextRenewal(client.getNextRenewal());
			clientFound.setMembership(client.getMembership());
			
			clientUpdated = this.clientService.save(clientFound);
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Cliente Actualizado", clientUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteClient(@PathVariable Long id){
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			this.clientService.delete(id);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
	
	@PutMapping("/renewMembership")
	public ResponseEntity<?> renewMembership(@RequestParam Long id_client, @RequestParam Long id_membership){
		
		Membership membershipFound = this.membershipService.findOne(id_membership);
		Client clientFound = this.clientService.findOne(id_client);
		Client clientUpdated = null;
		Map<String, Object> response = new HashMap<>();
		
		if(membershipFound == null || clientFound == null ) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			clientFound.setMembership(membershipFound);
			clientFound.setNextRenewal(LocalDateTime.now().plusDays(membershipFound.getDuration()));
			
			clientUpdated = clientService.save(clientFound);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Siguiente renovacion", clientUpdated.getNextRenewal());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
		
	}

}
