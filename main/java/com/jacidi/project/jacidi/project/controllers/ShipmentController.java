package com.jacidi.project.jacidi.project.controllers;

import java.time.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.jacidi.project.jacidi.project.constant.GlobalConstant;
import com.jacidi.project.jacidi.project.entities.Product;
import com.jacidi.project.jacidi.project.entities.Shipment;
import com.jacidi.project.jacidi.project.services.ProductServiceImpl;
import com.jacidi.project.jacidi.project.services.ShipmentServiceImpl;

@RestController
@CrossOrigin("*")
@RequestMapping("/shipment")
public class ShipmentController {
	
	@Autowired
	private ShipmentServiceImpl shipmentService;
	
	@Autowired
	private ProductServiceImpl productService;
	
	@GetMapping("/show")
	public List<Shipment> showShipments(){
		return (List<Shipment>) this.shipmentService.findAll();
	}
	
	@PostMapping("/")
	public ResponseEntity<?> createShipment(@RequestBody Shipment shipment){
		
		Map<String, Object> response = new HashMap<>();
		Shipment shipmentNew = null;
		
		//toma la fecha actual
		LocalDateTime dateNow = LocalDateTime.now();
		LocalDate compareOne = LocalDate.of(dateNow.getYear(), dateNow.getMonth(), dateNow.getDayOfMonth());
		
		//recupera la fecha del campo nextRenewal del cliente asociado
		LocalDateTime dateNextRenewalClient = shipment.getClient().getNextRenewal();
		LocalDate compareTwo = LocalDate.of(dateNextRenewalClient.getYear(), dateNextRenewalClient.getMonth(), dateNextRenewalClient.getDayOfMonth());
		
		//comparar los dias que han pasado en caso de que la feccha sea antigua
		Period period = Period.between(compareTwo, compareOne);
		
		//verificar si la fecha de nextRenewal del cliente asociado ya paso
		if(period.getDays() > LocalDateTime.now().getDayOfMonth()) {
			response.put("Mensaje", "La fecha de la renovacion ya paso");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.EXPECTATION_FAILED);
		}
		
		// recupera la minima prioridad del cliente asociado
		Integer minPrio = shipment.getClient().getMembership().getPrio();
		
		/*itera los productos en la lista y verifica si su prioridad es mayor al client.membership.prio asociado
		 * si un producto no es mayor entonces devolvera un mensaje informativo */
		for(Product p: shipment.getProducts()) {
			if(p.getMinPrio() < minPrio) {
				response.put("Mensaje", "Solo se pueden agregar productos cuya prioridad sea mayor de " + minPrio);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.EXPECTATION_FAILED);
			}
		}
		
		try {
			shipmentNew = this.shipmentService.save(shipment);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("Mnesaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Envio", shipmentNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@PutMapping("/removeProduct")
	public ResponseEntity<?> removeProduct(@RequestParam Long id_product, @RequestParam Long id_shipment){
		
		Product productFound = this.productService.findOne(id_product);
		Shipment shipmentFound = this.shipmentService.findOne(id_shipment);
		Shipment shipmentUpdated = null;
		List<Shipment> shipmentsByProduct = this.shipmentService.removeProduct(productFound);
		Map<String, Object> response = new HashMap<>();
		
		if(shipmentFound == null || productFound == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			for(Shipment s: shipmentsByProduct) {
				if(s.getId() == shipmentFound.getId()) {
					shipmentFound.setProducts(s.getProducts());
				}
			}
			
			this.productService.delete(id_product);
			
			shipmentUpdated = this.shipmentService.save(shipmentFound);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Envio Actualizado", shipmentUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}

}
