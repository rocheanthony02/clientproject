package com.jacidi.project.jacidi.project.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.jacidi.project.jacidi.project.constant.GlobalConstant;
import com.jacidi.project.jacidi.project.entities.Product;
import com.jacidi.project.jacidi.project.services.ProductServiceImpl;

@RestController
@CrossOrigin("*")
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private ProductServiceImpl productService;
	
	@GetMapping("/show")
	public List<Product> showProducts(){
		return this.productService.findAll();
	}
	
	@GetMapping("/show/{id}")
	public ResponseEntity<?> showOneProduct(@PathVariable Long id){
		
		Product product = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			product = this.productService.findOne(id);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		if(product == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Product>(product, HttpStatus.OK);
		
	}
	
	@PostMapping("/")
	public ResponseEntity<?> createProduct(@RequestBody Product product){
		
		Product productNew = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			productNew = this.productService.save(product);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Nuevo Producto", productNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateProduct(@RequestBody Product product, @PathVariable Long id){
		
		Product productFound = this.productService.findOne(id);
		Product productUpdated = null;
		Map<String, Object> response = new HashMap<>();
		
		if(productFound == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			productFound.setName(product.getName());
			productFound.setCost(product.getCost());
			productFound.setMinPrio(product.getMinPrio());
			
			productUpdated = this.productService.save(productFound);
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Producto Actualizado", productUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable Long id){
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			this.productService.delete(id);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}

}
