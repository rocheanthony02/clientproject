package com.jacidi.project.jacidi.project.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.jacidi.project.jacidi.project.constant.GlobalConstant;
import com.jacidi.project.jacidi.project.entities.Membership;
import com.jacidi.project.jacidi.project.services.MembershipServiceImpl;

@RestController
@CrossOrigin("*")
@RequestMapping("/membership")
public class MembershipController {
	
	
	@Autowired
	private MembershipServiceImpl membershipService;
	
	@GetMapping("/show")
	public List<Membership> showMemberships(){
		return this.membershipService.findAll();
	}
	
	@GetMapping("/show/{id}")
	public ResponseEntity<?> showOneMembership(@PathVariable Long id){
		
		Membership membership = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			membership = this.membershipService.findOne(id);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		if(membership == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Membership>(membership, HttpStatus.OK);
		
	}
	
	@PostMapping("/")
	public ResponseEntity<?> createMembership(@RequestBody Membership membership){
		
		Membership membershipNew = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			membershipNew = this.membershipService.save(membership);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Membresia Nueva", membershipNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateMembership(@RequestBody Membership membership, @PathVariable Long id){
		
		Membership membershipFound = this.membershipService.findOne(id);
		Membership membershipUpdated = null;
		Map<String, Object> response = new HashMap<>();
		
		if(membershipFound == null) {
			response.put("Mensaje", GlobalConstant.MESSAGE_NOT_FOUND.getMensaje());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			membershipFound.setName(membership.getName());
			membershipFound.setPrio(membership.getPrio());
			membershipFound.setDuration(membership.getDuration());
			
			membershipUpdated = this.membershipService.save(membershipFound);
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		response.put("Membresia Actualizada", membershipUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteMembership(@PathVariable Long id){
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			this.membershipService.delete(id);
			
		}catch(DataAccessException e) {
			response.put("Mensaje", GlobalConstant.MENSSAGE_ERROR.getMensaje() + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Mensaje", GlobalConstant.MESSAGE_SUCCESS.getMensaje());
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}

}
